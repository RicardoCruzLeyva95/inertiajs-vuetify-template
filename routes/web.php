<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DistributionListController;
use App\Http\Controllers\MobileNumberController;
use App\Http\Controllers\SMSCampaingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function(){

    Route::redirect('/','dashboard');

    Route::resource('dashboard', DashboardController::class)->only('index');
    
    Route::prefix('sms')->name('sms.')->group(function(){
        Route::resource('campaings', SMSCampaingController::class);
    });

    // Route::prefix('whatsapp')->name('whatsapp.')->group(function(){
    //     Route::resource('campaings', WhatsAppCampaingController::class);
    // });

    Route::prefix('catalogues')->name('catalogues.')->group(function(){
        Route::resource('distribution-lists', DistributionListController::class);
        Route::resource('mobile-numbers', MobileNumberController::class);
    });

});
