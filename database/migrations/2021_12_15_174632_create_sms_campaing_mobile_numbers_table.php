<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSMSCampaingMobileNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_campaing_mobile_numbers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sms_campaing_id')->constrained('sms_campaings');
            $table->foreignId('distribution_list_id')->nullable()->constrained('distribution_lists');
            $table->foreignId('mobile_number_id')->constrained('mobile_numbers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_campaing_mobile_numbers');
    }
}
