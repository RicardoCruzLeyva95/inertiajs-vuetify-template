<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SMSCampaing extends Model
{
    protected $table = 'sms_campaings';

    public function mobile_numbers()
    {
        return $this->belongsToMany(MobileNumber::class,'sms_campaings_mobile_numbers');
    }
}
