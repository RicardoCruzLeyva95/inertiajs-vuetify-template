import Vue from 'vue'
import Vuetify from 'vuetify'
import es from 'vuetify/es5/locale/es'

Vue.use(Vuetify)

export default new Vuetify({
    icons: {
        iconfont: 'mdi'
    },
    lang: {
        locales: { es },
        current: 'es'
    },
    theme: {
        dark: false
    }
})
