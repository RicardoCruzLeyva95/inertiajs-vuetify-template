export default {
    data(){
        return {
            titlePage: null,
        }
    },
    mounted(){
        this.$setTitlePage()
    },
    methods: {
        $setTitlePage(){
            if (this.titlePage !== null) {
                this.$store.state.page.title = this.titlePage
            }
        }
    }
}