require('./bootstrap')

import Vue from 'vue'
window.Vue = Vue

Vue.prototype.route = route

import VueGates from 'vue-gates'
Vue.use(VueGates, /*{ superRole: 'super-admin', persistent: true }*/)

import mixins from './mixins/mixins'
Vue.mixin(mixins)

import { createInertiaApp } from '@inertiajs/inertia-vue'

import Layout from './Pages/Layout'
import store from './store/store'
import vuetify from './plugins/vuetify'

createInertiaApp({
    resolve: name => {
        const page = require(`./Pages/${name}`).default
        page.layout = page.layout || Layout
        return page
    },
    setup({ el, App, props }) {
        new Vue({
            render: h => h(App, props),
            store,
            vuetify,
        }).$mount(el)
    },
});

import { InertiaProgress } from '@inertiajs/progress'
InertiaProgress.init()
