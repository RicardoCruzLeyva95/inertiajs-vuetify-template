import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        drawer: true,
        page: {
            title: null,
        }
    },
    actions: {
        toggleDrawer({ commit, state }){
            commit('setDrawer', !state.drawer)
        }
    },
    mutations: {
        setDrawer(state, value){
            state.drawer = value
        }
    }
})
